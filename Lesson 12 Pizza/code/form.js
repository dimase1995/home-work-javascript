const nameForm = document.getElementById("name");
const phone = document.getElementById("phone");
const email = document.getElementById("email");

const validate = (p, v) => p.test(v);
let flag;
nameForm.addEventListener("change", (e) => {
  if (
    e.target.dataset.type === "name" &&
    validate(/^[a-яієї'-]+$/gi, nameForm.value)
  ) {
    nameForm.style.border = "4px solid green";
    flag = true;
  } else {
    nameForm.style.border = "5px solid red";
    flag = false;
  }
});

phone.addEventListener("change", (e) => {
  if (
    e.target.dataset.type === "phone" &&
    validate(/\+380\d{9}$/, phone.value)
  ) {
    phone.style.border = "4px solid green";
    flag = true;
  } else {
    phone.style.border = "5px solid red";
    flag = false;
  }
});

email.addEventListener("change", (e) => {
  if (
    e.target.dataset.type === "email" &&
    validate(/[a-z0-9]+@[a-z0-9]+\.[a-z]{2,4}$/, email.value)
  ) {
    email.style.border = "4px solid green";
    flag = true;
  } else {
    email.style.border = "5px solid red";
    flag = false;
  }
});

document.getElementById("btnSubmit").onclick = function () {
  if (flag === true) {
    document.location = "./thank-you/index.html";
  } else {
    document.getElementById("date").textContent = "Не вірно введенні дані!";
  }
};
document.getElementById("clear").onclick = function () {
  document.querySelectorAll("#form input").textContent = "";
  nameForm.style.border = "";
  phone.style.border = "";
  email.style.border = "";
  document.getElementById("date").textContent = "";
};
/*

*/
