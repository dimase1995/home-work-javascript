/* Напиши функцию map(fn, array), которая принимает на вход функцию и массив, и обрабатывает каждый элемент массива этой функцией, возвращая новый массив. */
function map(fn, array) {
  let arr = [];
  for (let i = 0; i < array.length; i++) {
    arr[i] = fn(array[i]);
  }
  return arr;
}

// function fn() {}
// map(array);
// map(fn);
/* Перепишите функцию, используя оператор '?' или '||'
Следующая функция возвращает true, если параметр age больше 18. В ином случае она задаёт вопрос confirm и возвращает его результат.
1	function checkAge(age) {
2	if (age > 18) {
3	return true;
4	} else {
5	return confirm('Родители разрешили?');
6	} } */

const age = prompt("Вкажіть ваш вік!");
function checkAge(age) {
  return age >= 18 ? true : confirm("Батьки дозволили?");
}
checkAge(age);

const age2 = prompt("Вкажіть ваш вік!");
function checkAge(age) {
  return age >= 18 || confirm("Батьки дозволили?");
}
checkAge(age2);

const a = 10;
const b = 10;
function sum() {
  const rez = a + b;
  document.getElementById("disp").innerHTML = rez;
  //   document.write(rez);
}
sum();
