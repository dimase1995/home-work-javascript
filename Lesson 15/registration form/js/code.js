let pageRegistr = document.createElement("div");
pageRegistr.classList.add("pageRegistr");
document.body.append(pageRegistr);

let img = document.createElement("img");
img.classList.add("img");
img.src = "./img/Figure.png";
pageRegistr.appendChild(img);

let form = document.createElement("div");
form.classList.add("form");
pageRegistr.appendChild(form);

let nameform = document.createElement("h2");
nameform.classList.add("registForm");
nameform.textContent = "Registration form";
form.appendChild(nameform);

//
let names = document.createElement("div");
names.classList.add("name");
names.textContent = "Name";
form.appendChild(names);
//

let flag;

const validate = (p, v) => p.test(v);
let nameInput = document.createElement("input");
nameInput.classList.add("mailInput");
nameInput.addEventListener("change", () => {
  if (validate(/[a-z-,.]/gi, nameInput.value)) {
    flag = true;
    nameInput.style.border = "4px solid green";
  } else {
    flag = false;
    nameInput.style.border = "5px solid red";
  }
});
form.appendChild(nameInput);

//

let birth = document.createElement("div");
birth.classList.add("birth");
birth.textContent = "Date Of Birth";
form.appendChild(birth);
let birthInput = document.createElement("input");
birthInput.classList.add("birthInput");

birthInput.addEventListener("change", () => {
  if (validate(/\d{2}-\d{2}-\d{4}$/g, birthInput.value)) {
    flag = true;
    birthInput.style.border = "4px solid green";
  } else {
    flag = false;
    birthInput.style.border = "5px solid red";
  }
});

form.appendChild(birthInput);

let parents = document.createElement("div");
parents.classList.add("parents");
parents.textContent = "Father’s/Mother’s Name";
form.appendChild(parents);
let parentsInput = document.createElement("input");
parentsInput.classList.add("parentsInput");

parentsInput.addEventListener("change", () => {
  if (validate(/[a-z-,.]/gi, parentsInput.value)) {
    flag = true;
    parentsInput.style.border = "4px solid green";
  } else {
    flag = false;
    parentsInput.style.border = "5px solid red";
  }
});

form.appendChild(parentsInput);

let email = document.createElement("div");
email.classList.add("mail");
email.textContent = "Email";
form.appendChild(email);
let emailInput = document.createElement("input");
emailInput.classList.add("mailInput");

emailInput.addEventListener("change", () => {
  if (validate(/[a-z0-9]+@[a-z0-9]+\.[a-z]{2,4}/, emailInput.value)) {
    flag = true;
    emailInput.style.border = "4px solid green";
  } else {
    flag = false;
    emailInput.style.border = "5px solid red";
  }
});

form.appendChild(emailInput);

let mobile = document.createElement("div");
mobile.classList.add("mob");
mobile.textContent = "Mobile No.";
form.appendChild(mobile);
let mobileInput = document.createElement("input");
mobileInput.classList.add("mobInput");

mobileInput.addEventListener("change", () => {
  if (validate(/\d{3}-\d{3}-\d{2}-\d{2}/, mobileInput.value)) {
    flag = true;
    mobileInput.style.border = "4px solid green";
  } else {
    flag = false;
    mobileInput.style.border = "5px solid red";
  }
});

form.appendChild(mobileInput);

let password = document.createElement("div");
password.classList.add("pass");
password.textContent = "Password";
form.appendChild(password);
let passInput = document.createElement("input");
passInput.classList.add("passInput");

passInput.addEventListener("change", () => {
  if (validate(/[a-z0-9]{4,10}/g, passInput.value)) {
    flag = true;
    passInput.style.border = "4px solid green";
  } else {
    flag = false;
    passInput.style.border = "5px solid red";
  }
});

form.appendChild(passInput);

let password2 = document.createElement("div");
password2.classList.add("pass2");
password2.textContent = "Re-enter Password";
form.appendChild(password2);
let passInput2 = document.createElement("input");
passInput2.classList.add("passInput2");

passInput2.addEventListener("change", () => {
  if (passInput.value === passInput2.value) {
    flag = true;
    passInput2.style.border = "4px solid green";
  } else {
    flag = false;
    passInput2.style.border = "5px solid red";
  }
});

form.appendChild(passInput2);

let homeNum = document.createElement("div");
homeNum.classList.add("homeNum");
homeNum.textContent = "home Number";
form.appendChild(homeNum);
let homeNumInput = document.createElement("input");
homeNumInput.classList.add("homeNumInput");

homeNumInput.addEventListener("change", () => {
  if (validate(/\d{3}-\d{3}-\d{2}-\d{2}/, homeNumInput.value)) {
    flag = true;
    homeNumInput.style.border = "4px solid green";
  } else {
    flag = false;
    homeNumInput.style.border = "5px solid red";
  }
});

form.appendChild(homeNumInput);

let btnSub = document.createElement("button");
btnSub.classList.add("btnSub");
btnSub.textContent = "SUBMIT";
form.appendChild(btnSub);

btnSub.onclick = function () {
  if (flag === true) {
    document.location =
      "https://www.google.com/imgres?imgurl=https%3A%2F%2Fwww.meme-arsenal.com%2Fmemes%2Fbedfc9ca010f2d5f866e9de5171bfb1f.jpg&imgrefurl=https%3A%2F%2Fwww.meme-arsenal.com%2Fcreate%2Fmeme%2F2297379&tbnid=_Jktfq5oO_FFfM&vet=12ahUKEwjFqsmQ17n9AhX86rsIHf29DHoQMygAegUIARCyAQ..i&docid=Q1DtYqSscDJ7rM&w=1800&h=1220&q=%D1%8D%D1%82%D0%BE%20%D1%83%D1%81%D0%BF%D0%B5%D1%85%20%D0%BC%D0%B5%D0%BC&ved=2ahUKEwjFqsmQ17n9AhX86rsIHf29DHoQMygAegUIARCyAQ#imgrc=_Jktfq5oO_FFfM&imgdii=Hx3fT-VV0ez16M";
  } else {
    document.location =
      "https://www.google.com/imgres?imgurl=https%3A%2F%2Fwww.elegantthemes.com%2Fblog%2Fwp-content%2Fuploads%2F2020%2F08%2F000-http-error-codes.png&imgrefurl=https%3A%2F%2Fwww.elegantthemes.com%2Fblog%2Fwordpress%2Fthe-ultimate-guide-to-common-http-error-codes&tbnid=Leystw72I66tAM&vet=12ahUKEwj24orB17n9AhU8micCHWQMCmEQMygAegUIARC7AQ..i&docid=G84NVnYGVQ80kM&w=1080&h=608&q=error&ved=2ahUKEwj24orB17n9AhU8micCHWQMCmEQMygAegUIARC7AQ";
  }
};
