/*
* У папці calculator дана верстка макета калькулятора. 
Потрібно зробити цей калькулятор робочим.
* При натисканні на клавіші з цифрами - набір введених цифр має бути показаний на табло калькулятора.
* При натисканні на знаки операторів (`*`, `/`, `+`, `-`) на табло нічого не відбувається - програма чекає введення другого числа для виконання операції.

* Якщо користувач ввів одне число, вибрав оператор і ввів друге число, то при натисканні як кнопки `=`, так і будь-якого з операторів, 
в табло повинен з'явитися результат виконання попереднього виразу.

* При натисканні клавіш `M+` або `M-` у лівій частині табло необхідно показати маленьку букву `m` - це означає, що в пам'яті зберігається число.
 Натискання на MRC покаже число з пам'яті на екрані. Повторне натискання `MRC` має очищати пам'ять.
*/
let res = "";
let result;
const calculate = {
  operand1: "",
  operand2: "",
  sign: "",
  result: "",
  history: "",
  mrc: false,
  dot: false,
};

document.querySelector(".keys").addEventListener("click", (e) => {
  if (validate(/\d/, e.target.value) && calculate.sign === "") {
    calculate.operand1 += e.target.value;
    show(parseFloat(calculate.operand1));
  } else if (
    validate(/\./, e.target.value) &&
    calculate.operand1 !== "" &&
    calculate.operand2 === "" &&
    calculate.sign === ""
  ) {
    calculate.operand1 += e.target.value;
    show(calculate.operand1);
    calculate.dot = true;
    console.log("oper111");
  } else if (
    validate(/[-+/*]/, e.target.value) &&
    calculate.operand1 !== "" &&
    calculate.operand2 !== "" &&
    calculate.result === "" &&
    calculate.sign !== ""
  ) {
    calculateResult(calculate.operand1, calculate.operand2, calculate.sign);
    calculate.operand1 = calculate.result;
    calculate.operand2 = "";
    console.log("test1");
  } else if (
    validate(/[-+/*]/, e.target.value) &&
    calculate.operand1 !== "" &&
    calculate.operand2 !== "" &&
    calculate.result !== "" &&
    calculate.sign !== ""
  ) {
    calculateResult(calculate.operand1, calculate.operand2, calculate.sign);
    calculate.operand1 = calculate.result;
    calculate.operand2 = "";
    console.log("test2");
  } else if (validate(/[-+/*]/, e.target.value)) {
    calculate.sign = e.target.value;
    console.log("OPERATOR");
  } else if (
    validate(/\d/, e.target.value) &&
    calculate.sign !== "" &&
    calculate.operand1 !== ""
  ) {
    calculate.operand2 += e.target.value;
    show(parseFloat(calculate.operand2));
    document.getElementById("res").disabled = false;
    console.log("Operator22222");
  } else if (
    validate(/\./, e.target.value) &&
    calculate.operand1 !== "" &&
    calculate.operand2 !== "" &&
    calculate.result === "" &&
    calculate.sign !== ""
  ) {
    calculate.operand2 += e.target.value;
    show(calculate.operand2);
    calculate.dot = true;
    console.log("!!!!!");
  } else if (
    (validate(/\=/), e.target.value) &&
    calculate.operand1 !== "" &&
    calculate.operand2 !== "" &&
    calculate.sign !== ""
  ) {
    console.log("=========");
    calculateResult(calculate.operand1, calculate.operand2, calculate.sign);
    calculate.operand1 = calculate.result;
    calculate.operand2 = "";
  }

  console.log(calculate);
});
const calculateResult = (val1, val2, sign) => {
  switch (sign) {
    case "+":
      calculate.result = +val1 + +val2;
      show(calculate.result);
      break;
    case "-":
      calculate.result = val1 - val2;
      show(calculate.result);
      break;
    case "*":
      calculate.result = val1 * val2;
      show(calculate.result);
      break;
    case "/":
      calculate.result = val1 / val2;
      show(calculate.result);
      break;
  }
};
const disp = document.querySelector(".display");
const validate = (pattern, value) => pattern.test(value);
const display = document.querySelector(".display > input");
function show(data) {
  display.value = data;
}

function clearAll() {
  calculate.operand1 = "";
  calculate.operand2 = "";
  calculate.sign = "";
  calculate.result = "";
  calculate.history = "";
  show("0");
  minus.textContent = "";
}
document.getElementById("clear").onclick = clearAll;
//
/*
 При натисканні клавіш `M+` або `M-` у лівій частині табло необхідно показати маленьку букву `m` - це означає, що в пам'яті зберігається число.
 Натискання на MRC покаже число з пам'яті на екрані. Повторне натискання `MRC` має очищати пам'ять.
*/

const minus = document.createElement("div");
const mrc = document.getElementById("mrc");
//
function showM() {
  calculate.history = calculate.result;

  minus.classList.add("minus");
  minus.textContent = "m";
  disp.appendChild(minus);
}
document.getElementById("btnmmin").onclick = function () {
  showM();
};
document.getElementById("btnmplus").onclick = function () {
  showM();
};
mrc.onclick = function () {
  calculate.mrc
    ? ((calculate.history = ""), (calculate.mrc = false))
    : ((calculate.mrc = true),
      (minus.textContent = ""),
      show(calculate.history));
};
