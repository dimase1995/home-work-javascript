export const searchCatalogPage = (value, products = []) => {
  //   console.log(products);
  const l = products.length;
  //filter elements
  const arr = products
    .filter((product) => {
      return product.productName
        .toLowerCase()
        .includes(value.toLocaleLowerCase());
    })
    .map((product) => {
      return {
        productImg: product.availableOptions[0].optionImages[0],
        productName: product.productName,
      };
    });
  //   console.log(arr);
  showProductSearchList(arr, l);
};

//показ продукції в пошуку

function showProductSearchList(product = [], l) {
  const ul = document.querySelector(".product-list");
  ul.innerHTML = "";
  //щоб ховався список пошуку при видаленні символів в input
  if (product.length >= l) {
    ul.innerHTML = "";
    return;
  }
  // ul > li = product img + product name

  product.forEach(({ productImg, productName }) => {
    const li = document.createElement("li");
    const img = document.createElement("img");
    const p = document.createElement("p");
    img.src = productImg;
    img.alt = productName;
    p.innerText = productName;
    li.append(img, p);
    ul.append(li);
  });
}
