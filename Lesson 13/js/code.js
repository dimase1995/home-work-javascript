/*
 Створіть сайт з коментарями. Коментарі тут : https://jsonplaceholder.typicode.com/
    Сайт має виглядати так : https://kondrashov.online/images/screens/120.png
    На сторінку виводити по 10 коментарів, у низу сторінки зробити поле пагінації
     (перемикання сторінок)
     при перемиканні сторінок показувати нові коментарі. 
    з коментарів виводити : 
    "id": 1,
    "name"
    "email"
    "body":

    //
    ajax.onreadystatechange = function () {
  if (ajax.readyState == 4 && ajax.status == 200) {
    console.log(ajax);
  } else {
    console.log("error");
  }
};
*/
/*
const xhr = new XMLHttpRequest();
xhr.open("GET", "https://jsonplaceholder.typicode.com/comments");
xhr.onreadystatechange = function () {
  if (xhr.readyState !== 4 || xhr.status !== 200) {
    return;
  }
  const response = xhr.response;
  //   console.log(response);
  const card = (document.getElementById("card").innerHTML = response);
  console.log(card);
  function show(data = []) {
    data.forEach(({ id, name, email, body }, index) => {
      card.insertAdjacentHTML(
        "beforeend",
        `
              <p>${id}</p>
              <p>${name}</p>
              <p>${email}</p>
              <p>${body}</p>
              
              `
      );
    });
  }
};
xhr.send();
show();
*/
/*
function show(data = []) {
    if (!Array.isArray(data)) return;

    const card = (document.getElementById("card").innerHTML = response);
    //   card.innerHTML = "";
    const newArr = data.map(({ id, name, email, body }, i) => {
      return {
        id: i + 1,
        name: name,
        email: email,
        body: body,
      };
    });

    newArr.forEach(({ id, name, email, body }) => {
      card.insertAdjacentHTML(
        "beforeend",
        `
          <p>${id}</p>
          <p>${name}</p>
          <p>${email}</p>
          <p>${body}</p>
          
          `
      );
    });
  }
};
xhr.send();
show();



//
//
//

*/
const card = document.getElementById("card");
const content = document.getElementById("content");
function getServer(url, callback = () => {}) {
  const ajax = new XMLHttpRequest();
  ajax.open("get", url);
  ajax.send();
  ajax.addEventListener("readystatechange", () => {
    if (ajax.readyState === 4 && ajax.status >= 200 && ajax.status < 300) {
      callback(JSON.parse(ajax.response));
    } else if (ajax.readyState === 4) {
      throw new Error(
        `Помилка у запиті на сервер : ${ajax.status} / ${ajax.statusText}`
      );
    }
  });
}

function showExchange(data = []) {
  data.forEach(({ id, name, email, body }, index) => {
    const div = `
        <div> 
        <p>${id}</p>
        <p>${name}</p>
        <p>${email}</p>
        <p>${body}</p>
        </div>`;
    card.insertAdjacentHTML("beforeend", div);
    console.log(div);
  });
}
let numberOfPage = 0;
const listing_table = document.getElementById("card");

const nextButton = (document.querySelector(".slider-next").onclick =
  function () {
    listing_table.innerHTML = "";
    numberOfPage += 10;
    getServer(
      `https://jsonplaceholder.typicode.com/comments?_start=${numberOfPage}&_limit=10`,
      showExchange
    );
  });

const prevButton = (document.querySelector(".slider-prev").onclick =
  function () {
    if (numberOfPage > 0) {
      listing_table.innerHTML = "";
      numberOfPage -= 10;
      getServer(
        `https://jsonplaceholder.typicode.com/comments?_start=${numberOfPage}&_limit=10`,
        showExchange
      );
    }
  });

getServer(
  `https://jsonplaceholder.typicode.com/comments?_start=${numberOfPage}&_limit=10`,
  showExchange
);
