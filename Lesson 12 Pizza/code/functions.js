import { pizzaSelectUser } from "./index.js";
import pizza from "./pizza.js";
import { dragAndDrop } from "./dragdrop.js";
import { showInfo } from "./dragdrop.js";
function userSlectTopping(topping) {
  //size = "big"

  if ("smallmidbig".includes(topping)) {
    pizzaSelectUser.size = pizza.size.find((el) => {
      return el.name === topping;
    });
  } else if ("moc1moc2moc3telyavetch1vetch2".includes(topping)) {
    pizzaSelectUser.topping.push(
      pizza.topping.find((el) => el.name === topping)
    );
  } else if ("sauceClassicsauceBBQsauceRikotta".includes(topping)) {
    pizzaSelectUser.sauce = pizza.sauce.find((el) => el.name === topping);
    console.log(pizza.sauce.find((el) => el.name === topping));
    //document.getElementById("sauce").innerHTML += `<div> ${topping} </div> `;
  }
  pizzaSelectUser.price = show(pizzaSelectUser);
}

function show(pizza) {
  let price = 0;
  console.log(pizza);
  if (pizza.size) {
    price = price + pizza.size.price;
  }
  if (pizza.sauce.length > 0) {
    // price += pizza.sauce.price;
    // pizza.sauce.forEach(sauce => price + sauce.price)
    price += pizza.sauce.reduce((a, b) => {
      return a + b.price;
    }, 0);
  }
  if (pizza.topping.length > 0) {
    price += pizza.topping.reduce((a, b) => {
      return a + b.price;
    }, 0);
  }

  document.getElementById("price").textContent = price;
  //
  /*
  const toppingShow = document.getElementById("toppin");
  let HTMLElem = pizzaSelectUser.topping.map(
    (el) => ` <div>${el.productName}</div> `
  );
  toppingShow.innerHTML = HTMLElem.join("");
  */
  return price;
}

export { userSlectTopping };
export { show };
