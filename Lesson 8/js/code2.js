/*
ДЗ : Згенерувати  теги через javascript. Додати на сторінку семантичні теги та метагеги опису сторінки.
 прописати стилі  для елементів використовуючи css id та класи
 при натиску на тег ми можемо доти будь-який контент і він зберігається в тегу
https://ru.megaindex.com/blog/files/images/semantic-markup-example.jpg
*/
let header = document.createElement("header");
header.textContent = "HEADER";
header.classList.add("top");
document.body.append(header);
header.addEventListener("click", function (e) {
  let newHeader = prompt("Напишіть заголовок сторінки", "header");
  e.target.childNodes[0].textContent = `${newHeader}`;
  e.stopPropagation();
});

let nav = document.createElement("nav");
nav.textContent = "NAVIGATION";
nav.classList.add("navigation");
header.append(nav);
nav.addEventListener("click", function (elem) {
  let newNav = prompt("Напишіть navigation", "navigation");
  elem.target.childNodes[0].textContent = `${newNav}`;
  elem.stopPropagation();
});
let page = document.createElement("div");
page.classList.add("page");
document.body.append(page);

//
let section = document.createElement("section");
section.textContent = "SECTION";
section.classList.add("content");
page.append(section);
section.addEventListener("click", function (e) {
  let newSection = prompt("Напишіть контент сторінки", "section");
  e.target.childNodes[0].textContent = `${newSection}`;
  e.stopPropagation();
});

let headerContent = document.createElement("header");
headerContent.textContent = "HEADER";
headerContent.classList.add("headerContent");
section.append(headerContent);
nav.addEventListener("click", function (elem) {
  let newheaderContent = prompt(
    "Напишіть заголовок контента",
    "header Content"
  );
  elem.target.childNodes[0].textContent = `${newheaderContent}`;
  elem.stopPropagation();
});

let article = document.createElement("div");
article.textContent = "ARTICLE";
article.classList.add("article");
section.append(article);
article.addEventListener("click", function (e) {
  let newArticle = prompt("Напишіть статтю сторінки", "article");
  e.target.childNodes[0].textContent = `${newArticle}`;
  e.stopPropagation();
});

let headerArticle = document.createElement("header");
headerArticle.textContent = "HEADER";
headerArticle.classList.add("headerArticle");
article.append(headerArticle);
headerArticle.addEventListener("click", function (elem) {
  let newHeaderArticle = prompt("Напишіть заголовок статті", "header article");
  elem.target.childNodes[0].textContent = `${newHeaderArticle}`;
  elem.stopPropagation();
});
let paragraph1 = document.createElement("p");
paragraph1.textContent = "PARAGRAPH";
paragraph1.classList.add("text1");
article.append(paragraph1);
paragraph1.addEventListener("click", function (elem) {
  let newParagraph1 = prompt("Напишіть текст статті", "paragraph");
  elem.target.childNodes[0].textContent = `${newParagraph1}`;
  elem.stopPropagation();
});
let paragraph2 = document.createElement("p");
paragraph2.textContent = "PARAGRAPH";
paragraph2.classList.add("text2");
article.append(paragraph2);
paragraph2.addEventListener("click", function (elem) {
  let newParagraph2 = prompt("Напишіть текст статті", "paragraph");
  elem.target.childNodes[0].textContent = `${newParagraph2}`;
  elem.stopPropagation();
});
let aside = document.createElement("p");
aside.textContent = "ASIDE";
aside.classList.add("text3");
paragraph2.append(aside);
aside.addEventListener("click", function (elem) {
  let newAside = prompt("Напишіть текст статті", "aside");
  elem.target.childNodes[0].textContent = `${newAside}`;
  elem.stopPropagation();
});
let footerArticle = document.createElement("footer");
footerArticle.textContent = "FOOTER";
footerArticle.classList.add("footer1");
article.append(footerArticle);
footerArticle.addEventListener("click", function (elem) {
  let newFooter1 = prompt("Напишіть footer статті", "footer");
  elem.target.childNodes[0].textContent = `${newFooter1}`;
  elem.stopPropagation();
});
//two part
let article2 = document.createElement("div");
article2.textContent = "ARTICLE";
article2.classList.add("article2");
section.append(article2);
article2.addEventListener("click", function (e) {
  let newArticle2 = prompt("Напишіть статтю сторінки", "article");
  e.target.childNodes[0].textContent = `${newArticle2}`;
  e.stopPropagation();
});
let headerArticle2 = document.createElement("header");
headerArticle2.textContent = "HEADER";
headerArticle2.classList.add("headerArticle2");
article2.append(headerArticle2);
headerArticle2.addEventListener("click", function (elem) {
  let newHeaderArticle2 = prompt("Напишіть заголовок статті", "header article");
  elem.target.childNodes[0].textContent = `${newHeaderArticle2}`;
  elem.stopPropagation();
});
let paragraph3 = document.createElement("p");
paragraph3.textContent = "PARAGRAPH";
paragraph3.classList.add("text4");
article2.append(paragraph3);
paragraph3.addEventListener("click", function (elem) {
  let newParagraph3 = prompt("Напишіть текст статті", "paragraph");
  elem.target.childNodes[0].textContent = `${newParagraph3}`;
  elem.stopPropagation();
});
let paragraph4 = document.createElement("p");
paragraph4.textContent = "PARAGRAPH";
paragraph4.classList.add("text5");
article2.append(paragraph4);
paragraph4.addEventListener("click", function (elem) {
  let newParagraph4 = prompt("Напишіть текст статті", "paragraph");
  elem.target.childNodes[0].textContent = `${newParagraph4}`;
  elem.stopPropagation();
});
let footerArticle2 = document.createElement("footer");
footerArticle2.textContent = "FOOTER";
footerArticle2.classList.add("footer2");
article2.append(footerArticle2);
footerArticle2.addEventListener("click", function (elem) {
  let newFooter2 = prompt("Напишіть footer статті", "footer");
  elem.target.childNodes[0].textContent = `${newFooter2}`;
  elem.stopPropagation();
});
//
let footer = document.createElement("footer");
footer.textContent = "FOOTER";
footer.classList.add("footer");
section.append(footer);
footer.addEventListener("click", function (elem) {
  let newFooter = prompt("Напишіть footer статті", "footer");
  elem.target.childNodes[0].textContent = `${newFooter}`;
  elem.stopPropagation();
});
//right section
let rightSection = document.createElement("section");
rightSection.textContent = "SECTION";
rightSection.classList.add("rightSection");
page.append(rightSection);
rightSection.addEventListener("click", function (e) {
  let newRightSection = prompt("Напишіть контент сторінки", "right section");
  e.target.childNodes[0].textContent = `${newRightSection}`;
  e.stopPropagation();
});
let rightHeader = document.createElement("header");
rightHeader.textContent = "HEADER";
rightHeader.classList.add("rightHeader");
rightSection.append(rightHeader);
rightHeader.addEventListener("click", function (elem) {
  let newRightHeader = prompt("Напишіть заголовок контента", "right header");
  elem.target.childNodes[0].textContent = `${newRightHeader}`;
  elem.stopPropagation();
});
let rightNav = document.createElement("nav");
rightNav.textContent = "NAV";
rightNav.classList.add("rightNav");
rightSection.append(rightNav);
rightNav.addEventListener("click", function (elem) {
  let newRightNav = prompt("Напишіть navigation", "navigation");
  elem.target.childNodes[0].textContent = `${newRightNav}`;
  elem.stopPropagation();
});

//
let downFooter = document.createElement("footer");
downFooter.textContent = "FOOTER";
downFooter.classList.add("downFooter");
document.body.append(downFooter);
downFooter.addEventListener("click", function (e) {
  let newdownFooter = prompt("Напишіть заголовок сторінки", "header");
  e.target.childNodes[0].textContent = `${newdownFooter}`;
  e.stopPropagation();
});
