//вивід на головну сторінку 4 випадкові карточки продукту
export function randomProduct(product, quantity, math = Math) {
  //перевірка чи масив
  if (!Array.isArray(product)) {
    console.warn("Не масив!");
    return;
  }
  if (quantity >= product.length) {
    console.warn(
      "Кількість випадкових елементів перевищює кількість елементів масиву"
    );
    return;
  }

  let indexArr = [];
  for (let i = 0; i < quantity; i++) {
    let indexRandom = math.floor(math.random() * product.length);
    //щоб не було співпадіння
    while (indexArr.includes(indexRandom)) {
      indexRandom = math.floor(math.random() * product.length);
    }
    indexArr.push(indexRandom);
    // console.log(indexRandom);
    // console.log(indexArr);
  }
  //перебір масив з 5 елементів
  return indexArr.map((el) => {
    return product[el];
  });
}
