/*
Намалювати на сторінці коло за допомогою параметрів, які введе користувач.
 При завантаженні сторінки – показати на ній кнопку з текстом "Намалювати коло". 
 Дана кнопка повинна бути єдиним контентом у тілі HTML документа, 
 решта контенту повинен бути створений і доданий на сторінку за допомогою Javascript
 При натисканні кнопки "Намалювати коло" показувати одне поле введення - діаметр кола. 
 При натисканні на кнопку "Намалювати" створити на сторінці 100 кіл (10*10) випадкового кольору. 
 При натисканні на конкретне коло - це коло повинен зникати, при цьому порожнє місце заповнюватися, тобто всі інші кола зрушуються вліво. 
*/

document.getElementById("button").addEventListener("click", function () {
  let box = document.createElement("div");
  box.classList.add("box");
  document.body.appendChild(box);
  let paint = document.createElement("button");
  paint.textContent = "НАМАЛЮВАТИ";
  paint.classList.add("paint");
  box.append(paint);

  paint.addEventListener("click", function () {
    box.style.display = "none";
    let content = document.createElement("div");
    content.classList.add("content");
    document.body.append(content);
    function getRandomInt(min, max) {
      min = Math.ceil(min);
      max = Math.floor(max);
      return Math.floor(Math.random() * (max - min + 1) + min);
    }
    let circle = 100;
    for (let i = 0; i < circle; i++) {
      let kolo = document.createElement("div");
      kolo.classList.add("kolo");
      kolo.style.backgroundColor = `rgb(${getRandomInt(0, 255)}, ${getRandomInt(
        0,
        255
      )}, ${getRandomInt(0, 255)} )`;
      content.appendChild(kolo);
    }
    let fil = document.getElementsByClassName("kolo");
    for (let i = 0; i < fil.length; i++) {
      fil[i].addEventListener("click", hide);
    }
    function hide() {
      this.style.display = "none";
    }
  });
});
