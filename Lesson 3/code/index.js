// Дані два масиви: ['a', 'b', 'c'] та [1, 2, 3]. Об'єднайте їх разом.
let a = ["a", "b", "c"];
let b = a.concat(1, 2, 3);
document.write(b);
document.write("<br/>");
// Дан масив ['a', 'b', 'c']. Додайте йому до кінця елементи 1, 2, 3.
let c = ["a", "b", "c"];
c.push(1, 2, 3);
document.write(c);
document.write("<br/>");
// Дан масив [1, 2, 3]. Зробіть із нього масив [3, 2, 1].
let rev = [1, 2, 3];
rev.reverse();
document.write(rev);
document.write("<br/>");
// Дан масив [1, 2, 3]. Додайте йому до кінця елементи 4, 5, 6.
let l = [1, 2, 3];
l.push(4, 5, 6);
document.write(l);
document.write("<br/>");
// Дан масив [1, 2, 3]. Додайте йому на початок елементи 4, 5, 6.
let arr = [1, 2, 3];
arr.unshift(4, 5, 6);
document.write(arr);
document.write("<br/>");
// Дан масив ['js', 'css', 'jq']. Виведіть перший елемент на екран.
let d = ["js", "css", "jq"];
document.write(d[0]);
document.write("<br/>");
// Дан масив [1, 2, 3, 4, 5]. За допомогою методу slice запишіть нові елементи [1, 2, 3].
let k = [1, 2, 3, 4, 5];
let newElem = k.slice(0, 3);
document.write(newElem);
document.write("<br/>");
// Дан масив [1, 2, 3, 4, 5]. За допомогою методу splice перетворіть масив на [1, 4, 5]
let s = [1, 2, 3, 4, 5];
let del = s.splice(1, 2);
document.write(s);
document.write("<br/>");
// Дан масив [1, 2, 3, 4, 5]. За допомогою методу splice перетворіть масив на [1, 2, 10, 3, 4, 5].
let m = [1, 2, 3, 4, 5];
m.splice((m.length - 1) / 2, 0, 10);
document.write(m);
document.write("<br/>");
// Дан масив [3, 4, 1, 2, 7]. Відсортуйте його.
let e = [3, 4, 1, 2, 7];
e.sort();
document.write(e);
document.write("<br/>");
// Дан масив з елементами 'Привіт, ', 'світ' і '!'. Потрібно вивести на екран фразу 'Привіт, мир!'
let hello = ["Привіт, ", "світ", "!"];
hello.splice(1, 1, "Мир");
document.write(hello.join(""));
document.write("<br/>");
// Дан масив ['Привіт, ', 'світ', '!']. Необхідно записати в нульовий елемент цього масиву слово 'Поки, ' (тобто замість слова 'Привіт, ' буде 'Поки, ').
let helloWorld = ["Привіт, ", "світ", "!"];
helloWorld[0] = "Поки, ";
document.write(helloWorld.join(""));
document.write("<br/>");
// Створіть масив arr з елементами 1, 2, 3, 4, 5 двома різними способами.
const one = [1, 2, 3, 4, 5];
let two = new Array(1, 2, 3, 4, 5);

// Дан багатовимірний масив arr:
var arr3 = {
  ru: ["блакитний", "червоний", "зелений"],
  en: ["blue", "red", "green"],
};
//         Виведіть за його допомогою слово 'блакитний' 'blue' .
document.write(arr3.ru[0] + "<br/>");
document.write(arr3.en[0]);
document.write("<br/>");
// Створіть масив arr = ['a', 'b', 'c', 'd'] і за допомогою його виведіть на екран рядок 'a+b, c+d'.
let arr2 = ["a", "b", "c", "d"];
let dell = arr2.splice(2, 2);
let newArr = arr2.join("+") + dell.join("+");
document.write(newArr);
document.write("<br/>");
// Запитайте у користувача кількість елементів масиву. Виходячи з даних, які ввів користувач
// створіть масив на ту кількість елементів, яку передав користувач.
// у кожному індексі масиву зберігайте чило який показуватиме номер елемента масиву.
const msg = parseFloat(prompt("Введіть число!"));
const arr4 = [msg];
for (let i = 0; i <= arr4; i++) {
  if (i % 2 !== 0) {
    document.write(i);
  } else if (i % 2 !== 1) {
    document.write(i);
  }
}
document.write("<br/>");
// Зробіть так, щоб з масиву, який ви створили вище, вивелися всі непарні числа в параграфі, а парні в спані з червоним тлом.

// Напишіть код, який перетворює та об'єднує всі елементи масиву в одне рядкове значення. Елементи масиву будуть розділені комою.
let vegetables = ["Капуста", "Ріпа", "Редиска", "Морквина"];
let str1 = vegetables.join(", ");
document.write(str1);
document.write("<br/>");
//                 <span class="green">// Ваш код</span>
//                 document.write(str1); // "Капуста, Ріпа, Редиска, Морквина"
