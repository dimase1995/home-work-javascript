// генератор html тегів з тексту в розмітку
/*
Робимо роботу до 3х владеностей (дії). Тобто не більге ніж 
        .one>div^a - видасть 
        <div class="one">
            <div></div>
        </div>
        <a href=""></a>
        .one>div^a>span - вже помилка.

        . - класс
        # - id
        > 1й рівень вкладеності
        div - та інші назви тегів
        {} - текст
        на навій строці нові інструкуці (Якщо в тексейрі перейшли на нову строку, 
            то видаємо як нову вкладеність)
        */

const body = document.querySelector("body");
const section = document.querySelector(".result");
const textArea = document.querySelector(".textArea");

const obj = {
  tag: "",
  tag2: "",
  class: "",
  id: "",
  innerHTML: "",
  test: "",
  test2: "",
  test3: "",
};
let result;
//
const validate = (pattern, value) => pattern.test(value);
let sign = [".", "#", ">", "^", "{}"];
//
textArea.addEventListener(
  "keyup",
  (makeAction = (e) => {
    text = e.target.value;
    //   show(text);
    if (e.code === "Enter") {
      console.log("ENTER");
      if (validate(/.+[a-z]\>[a-z]+\^[a-z]/g, text)) {
        let arr = text.split(".");
        obj.innerHTML = arr[1];
        obj.class = obj.innerHTML.split(">");
        obj.tag = obj.class[0];
        obj.tag2 = obj.class[1];
        obj.test = obj.tag2.split("^");
        obj.test2 = obj.test[0];
        obj.test3 = obj.test[1];
        show(`<div class="${obj.tag}">
        <${obj.test2}></${obj.test2}>
    </div>
    <${obj.test3}></${obj.test3}>`);
        clearCacheValues();
        arr = [];
      } else if (validate(/[a-z]\>[a-z]/, text)) {
        let arr = text.split(">");
        obj.tag = arr[0];
        obj.tag2 = arr[1];
        show(`<${obj.tag}>
        <${obj.tag2}></${obj.tag2}>
        <${obj.tag}>`);
        clearCacheValues();
        arr = [];
      } else if (validate(/[a-z]\.[a-z]/, text)) {
        console.log("INSIDE IF");
        let arr = text.split(".");
        obj.tag = arr[0];
        obj.class = arr[1];
        show(`<${obj.tag} class="${obj.class}"></${obj.tag}>`);
        clearCacheValues();
        arr = [];
      } else if (validate(/[a-z]\#[a-z]/, text)) {
        let arr = text.split("#");
        obj.tag = arr[0];
        obj.id = arr[1];
        show(`<${obj.tag} id="${obj.id}"></${obj.tag}>`);
        clearCacheValues();
        arr = [];
      } else if (validate(/[a-z]\{[{a-z}]/, text)) {
        let arr = text.split("{");
        obj.tag = arr[0];
        obj.innerHTML = arr[1];
        obj.test = obj.innerHTML.split("}");
        obj.test2 = obj.test[0];
        show(`<${obj.tag}>${obj.test2}</${obj.tag}>`);
        clearCacheValues();
        arr = [];
      } else if (validate(/#[a-z]+/, text)) {
        let arr = text.split("#");
        obj.tag = arr[0];
        obj.id = arr[1];
        show(`<div id='${obj.id}'> </div>`);
        clearCacheValues();
        arr = [];
      } else if (validate(/.[a-z]+/, text)) {
        let arr = text.split(".");
        obj.tag = arr[0];
        obj.class = arr[1];
        show(`<div class='${obj.class}'></div>`);
        clearCacheValues();
        arr = [];
        console.log("class");
      } else if (validate(/[a-z]/g, text)) {
        obj.innerHTML = text;
        show(`<${obj.innerHTML}></${obj.innerHTML}>`);
        clearCacheValues();
        console.log("text", `<${text}> </${text}>`);
      } else {
        show("Не доступно!");
      }
    }
  })
);
// text.substring(1)
const clearCacheValues = () => {
  for (let [key, value] of Object.entries(obj)) {
    key = "";
  }
  textArea.value = "";
};

function show(data) {
  if (!result) {
    section.value = data;
    result = data;
  } else {
    section.value = result + data;
  }
}
