/*
Виконати запит на https://swapi.dev/api/people (https://starwars-visualguide.com) 
отримати список героїв зіркових воєн.

Вивести кожного героя окремою карткою із зазначенням. картинки Імені, 
статевої приналежності, ріст, колір шкіри,
рік народження та планету на якій народився.

Створити кнопку зберегти на кожній картці. 
При натисканні кнопки записуйте інформацію у браузері
*/
const card = document.getElementById("card");
const content = document.getElementById("content");
function getServer(url, callback = () => {}) {
  const ajax = new XMLHttpRequest();
  ajax.open("get", url);
  ajax.send();
  ajax.addEventListener("readystatechange", () => {
    if (ajax.readyState === 4 && ajax.status >= 200 && ajax.status < 300) {
      callback(JSON.parse(ajax.response));
    } else if (ajax.readyState === 4) {
      throw new Error(
        `Помилка у запиті на сервер : ${ajax.status} / ${ajax.statusText}`
      );
    }
  });
}

function addToStorage(data) {
  const key = JSON.parse(data);
  localStorage.setItem(key.name, data);
}

function show(data = []) {
  const array = data.results;
  array.forEach(
    ({ name, height, skin_color, birth_year, gender, homeworld }, index) => {
      const div = `
        <div> 
        <div>${name}</div>
        <div class="photos"><img src="https://starwars-visualguide.com/assets/img/characters/${
          index + 1
        }.jpg" alt="photo"></div>
        <div>${height}</div>
        <div>${skin_color}</div>
        <div>${birth_year}</div>
        <div>${gender}</div>
        <div>${homeworld}</div>
        <button class='save' id='btn-${index}'>SAVE</button>
        </div>`;

      content.insertAdjacentHTML("beforeend", div);
      document.getElementById(`btn-${index}`).onclick = function () {
        addToStorage(JSON.stringify(array[index]));
      };
    }
  );
}

getServer("https://swapi.dev/api/people/?format=json", show);
