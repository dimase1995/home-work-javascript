import { FETCH } from "../../code/request.js";
import { searchCatalogPage } from "./search.js";

//SEARCH
const inputSearch = document.querySelector("[name='search-line']");
let productList = [];
let isInitCall = false;
//отримання інфо із сервера
let pagBtn = [];
function getProduct(data) {
  productList = data;
  pagBtn = createPaggination(productList);

  pagBtn.forEach((button) => {
    button.addEventListener("click", (e) => {
      changePage(e.target, productList);
    });
  });

  productList.slice(0, 10).forEach((element) => {
    document.querySelector(".content").append(createProductElement(element));
  });
  /*
  //добавлення карток по факту
  data.forEach((elem) => {
    // console.log(createProductElement(elem.availableOptions));
    document.querySelector(".content").append(createProductElement(elem));
  });
  */
  showFilterColorSize(getColorsSizeProducts(data));
  productList = data;
  if (!isInitCall) {
    initListeners();
    initListenersColor();
  }
  isInitCall = true;
}

function removeAllProducts() {
  const parentProductscontainer = document.querySelector(".content");
  while (parentProductscontainer.firstChild) {
    parentProductscontainer.removeChild(parentProductscontainer.firstChild);
  }
}

const displayFilteredProducts = (filteredProducts) => {
  removeAllProducts();
  filteredProducts.forEach((elem) => {
    document.querySelector(".content").append(createProductElement(elem));
  });
};

//створення шаблону для товарних карток через fn (так як маємо події),
//insertAdjacentHTML підїодить для читання - не для подій

function createProductElement(product) {
  const card = document.createElement("div");
  card.classList.add("cards");
  const info = `
          <img class="img-card" src="${
            product.availableOptions[0].optionImages[0]
          }" alt="${product.productName}" />
          <div class="name-product">${product.productName}</div>
          <img  src="./img/Group 1 star.png" alt="logo" />
          <div class="price-product">As low as UAH${
            product.availableOptions[0].prices[0].price
          }</div>
          <div class="colors">
          ${product.availableOptions.map((el) => {
            return `<div data-id="${el.option_id}" style="background-color:#${el.optionColorCode}"></div>`;
          })}</div>
          <button class="btn-add" data-id="${
            product.id
          }">🛒 ADD TO CARD</button>
    `;
  card.insertAdjacentHTML("beforeend", info);
  // console.log(product);
  return card;
}

//фільтрація кольорів and SIZE у вкладці фільтр
//витяг кольорів із бази даних (масив)
const getColorsSizeProducts = (products = []) => {
  if (!Array.isArray(products)) return;
  const mainColorArr = [];
  const mainSizeArr = [];

  products.forEach((el) => {
    //console.log(el.availableOptions);
    el.availableOptions.forEach((color) => {
      if (!mainColorArr.includes(color.optionColorCode)) {
        mainColorArr.push(color.optionColorCode);
      }
      //console.log(e.optionColorCode);
      // console.log(mainColorArr);
      // console.log(mainSizeArr);
    });
  });

  products.forEach((el) => {
    // console.log(el.availableOptions);
    el.availableOptions.forEach((option) => {
      option.prices.forEach((sizeElem) => {
        if (!mainSizeArr.includes(sizeElem.size)) {
          mainSizeArr.push(sizeElem.size);
          // console.log(sizeElem.size);
        }
      });

      //console.log(e.optionColorCode);
      // console.log(mainColorArr);
    });
  });
  return { color: mainColorArr, size: mainSizeArr };
  //products.availableOptions
};
let showSize = [];
function showFilterColorSize(option) {
  // console.log(option);
  const elemColor = document.querySelector(".filter-parameters-color");
  const elemSize = document.querySelector(".filter-parameters-size");
  //colors
  if (elemColor.hasChildNodes()) {
    //console.log("HERE1");
  } else {
    option.color.forEach((color) => {
      const div = document.createElement("div");
      const checkboxLabel = document.createElement("label");
      const checkbox = document.createElement("input");
      checkbox.type = "checkbox";
      checkbox.className = "color-parameter";
      checkboxLabel.classList.add("colors_parameters");
      checkboxLabel.style.backgroundColor = `#${color}`;
      checkboxLabel.htmlFor = `#${color}`;
      checkboxLabel.innerHTML = `#${color}`;
      checkbox.id = `#${color}`;
      checkbox.name = `#${color}`;
      div.append(checkbox, checkboxLabel);
      elemColor.append(div);
    });
  }
  //size
  if (elemSize.hasChildNodes()) {
    //console.log("HERE2");
  } else {
    option.size.forEach((size) => {
      showSize.push(size);
      const targetDiv = document.createElement("div");
      const checkboxLabel = document.createElement("label");
      const checkbox = document.createElement("input");
      checkboxLabel.htmlFor = size;
      checkboxLabel.innerHTML = size;
      checkbox.type = "checkbox";
      checkbox.className = "size-parameter";
      checkbox.id = size;
      checkbox.name = size;
      // checkbox.classList.add("size-parameter");
      targetDiv.append(checkbox, checkboxLabel);
      elemSize.append(targetDiv);
      // console.log(size);
    });
  }
}

//

//SEARCH

inputSearch.addEventListener("input", (e) => {
  searchCatalogPage(e.target.value, productList);
});

//
FETCH("https://store-demo-be.onrender.com/api/products", getProduct);

//
//Фільтрація по кліку на SIZE
let filtSiz = [];
const initListeners = () => {
  document.querySelectorAll(".size-parameter").forEach((div) => {
    div.addEventListener("change", (e) => clickSize(e));
  });
};

const clickSize = (e) => {
  userSlectSize(e.target.name);
};

function userSlectSize(size) {
  if (!filtSiz.includes(size)) {
    filtSiz.push(size);
  } else {
    const filteredArr = filtSiz.filter((s) => s !== size);
    filtSiz = filteredArr;
  }
  filterCardsBySize(filtSiz);
}

function filterCardsBySize(filtSiz) {
  if (filtSiz.length > 0) {
    const checkPriceCompatible = (element) => {
      return element.availableOptions.some((option) =>
        option.prices.find(
          (price) => price.size === filtSiz.find((size) => price.size)
        )
      );
    };

    const filteredProductOptions = productList.reduce((filtered, pr) => {
      if (checkPriceCompatible(pr)) {
        const filteredValue = pr;
        filtered.push(filteredValue);
      }
      return filtered;
    }, []);
    // console.log("options", filteredProductOptions);
    displayFilteredProducts(filteredProductOptions);
  } else {
    // console.log("IN ELSE STATEMENT", filtSiz);
    removeAllProducts();
    FETCH("https://store-demo-be.onrender.com/api/products", getProduct);
  }
}
//Фільтрація по кліку на COLORS
let filtColor = [];
const initListenersColor = () => {
  document.querySelectorAll(".color-parameter").forEach((div) => {
    div.addEventListener("change", (e) => clickColor(e));
  });
};

const clickColor = (e) => {
  userSlectColor(e.target.id);
};

function userSlectColor(color) {
  if (!filtColor.includes(color)) {
    filtColor.push(color);
  } else {
    const filteredArr = filtColor.filter((s) => s !== color);
    filtColor = filteredArr;
  }
  filterCardsByColor(filtColor);
}

function filterCardsByColor(filtColor) {
  if (filtColor.length > 0) {
    const filterSizesArrNoSymbol = filtColor.map((color) => color.slice(1));
    const checkColorCompatible = (element) => {
      return element.availableOptions.some(
        (option) =>
          option.optionColorCode ===
          filterSizesArrNoSymbol.find((colorCode) => option.optionColorCode)
      );
    };

    const filteredProductOptions = productList.reduce((filtered, pr) => {
      if (checkColorCompatible(pr)) {
        const filteredValue = pr;
        filtered.push(filteredValue);
      }
      return filtered;
    }, []);
    console.log("options", filteredProductOptions);
    displayFilteredProducts(filteredProductOptions);
  } else {
    // console.log("IN ELSE STATEMENT", filtColor);
    removeAllProducts();
    FETCH("https://store-demo-be.onrender.com/api/products", getProduct);
  }
}
//ПАГІНАЦІЯ
function createPaggination(data) {
  let itemsOnPage = 10;
  let countOfPages = Math.ceil(data.length / itemsOnPage);
  let pages = [];

  document.querySelector(".pagination").innerHTML = "";

  for (let i = 0; i < countOfPages; i++) {
    const div = document.createElement("div");
    div.classList.add("pag");
    div.innerText = i + 1;
    if (i === 0) {
      div.classList.add("pagination-selected");
    }
    document.querySelector(".pagination").append(div);
    pages.push(div);
  }
  return pages;
}
function changePage(target, data) {
  let itemsOnPage = 10;
  let numPage = +target.innerText;
  let startItem = (numPage - 1) * itemsOnPage;
  let endItem = startItem + itemsOnPage;

  document.querySelector(".content").innerHTML = "";

  if (!target.classList.contains("pag")) return;
  document.querySelectorAll(".pag").forEach((el) => {
    if (el.classList.contains("pagination-selected")) {
      el.classList.remove("pagination-selected");
    }
  });
  target.classList.add("pagination-selected");

  data.slice(startItem, endItem).forEach((element) => {
    document.querySelector(".content").append(createProductElement(element));
  });
}
//КОРЗИНА.............//////////////////////////

//відкрити корзину
document.querySelector(".basket").addEventListener("click", () => {
  document.location.pathname = "../basket/index.html";
});
export let id = [];

// додати в корзину товар
document.querySelector(".content").addEventListener("click", (e) => {
  if (e.target.classList.contains("btn-add")) {
    id.push(e.target.dataset.id);
    localStorage.idBD = JSON.stringify(id);

    alert("Товар додано до корзини!");
  }
});
