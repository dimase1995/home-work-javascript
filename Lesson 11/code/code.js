/*
1. 
Прив'яжіть усім інпутам наступну подію - втрата фокусу кожен інпут виводить своє value в параграф 
з id="test"
focus blur input
*/

const [...inputs] = document.querySelectorAll(".form > input");
inputs.forEach((input) => {
  input.addEventListener("focus", () => {
    input.className = "";
    input.classList.add("focus");
  });
  input.addEventListener("blur", () => {
    input.className = "";
    input.classList.add("blur");
    document.getElementById("output").innerHTML = input.value;
  });
});

/*
2.
Дано інпути. Зробіть так, щоб усі інпути втрати фокусу перевіряли свій вміст на правильну кількість 
символів. 
Скільки символів має бути в інпуті, зазначається в атрибуті data-length. 
Якщо вбито правильну кількість, то межа інпуту стає зеленою, якщо неправильна – червоною.
*/

const [...input] = document.querySelectorAll(".form2 > input");
input.forEach((input) => {
  input.addEventListener("focus", () => {
    input.className = "";
    input.classList.add("focus2");
  });
  input.addEventListener("blur", () => {
    if (input.value.length < input.dataset.length) {
      input.className = "";
      input.classList.add("blur2");
    } else {
      input.className = "";
      input.classList.add("erorr");
    }
  });
});
/*
4.
- При завантаженні сторінки показати користувачеві поле введення (`input`) з написом `Price`. 
Це поле буде служити для введення числових значень
- Поведінка поля має бути такою:
- При фокусі на полі введення – у нього має з'явитися рамка зеленого кольору. При втраті фокусу вона пропадає.
- Коли забрали фокус з поля - його значення зчитується, над полем створюється `span`, 
в якому має бути виведений текст: 
. 
Поруч із ним має бути кнопка з хрестиком (`X`). Значення всередині поля введення фарбується зеленим.
- При натисканні на `Х` - `span` з текстом та кнопка `X` повинні бути видалені.
- Якщо користувач ввів число менше 0 - при втраті фокусу підсвічувати поле введення червоною рамкою, 
під полем виводити фразу - `Please enter correct price`. `span` зі значенням при цьому не створюється.
*/
const button = document.getElementById("button");
const [...inputt] = document.querySelectorAll(".num > input");
inputt.forEach((input) => {
  input.addEventListener("focus", () => {
    input.classList.add("focus3");
    button.classList.add("buttFocus");
  });
  input.addEventListener("blur", () => {
    if (input.value > 0) {
      input.className = "";
      button.className = "";
      document.getElementById("inform").textContent = "";
    } else {
      input.className = "";
      button.className = "";
      input.classList.add("err");
      document.getElementById("inform").textContent =
        "Please enter correct price";
    }

    document.getElementById("info").innerHTML = input.value;
  });
});
button.onclick = function () {
  document.getElementById("info").innerHTML = "";
  document.querySelector(".num > input").value = "";
};
