/* Створіть на сторінці div і дайте йому зовнішній відступ 150 пікселів. Використовуючи JS виведіть у консоль відступ */
const div = document.getElementsByClassName("div");
console.log(div);
/*
Створіть програму секундомір.
* Секундомір матиме 3 кнопки "Старт, Стоп, Скидання"
* При натисканні на кнопку стоп фон секундоміра має бути червоним, старт - зелений, скидання - сірий * Виведення лічильників у форматі ЧЧ:ММ:СС
* Реалізуйте Завдання використовуючи синтаксис ES6 та стрілочні функції
*/
let intervalHandler,
  flag = false;
let min = 0;
let sec = 0;
let milisec = 0;
const get = (id) => document.getElementById(id);
const count = () => {
  get("milisec").innerHTML = milisec;
  milisec++;
  if (milisec == 99) {
    milisec = 0;
    sec++;
    get("sec").innerHTML = sec;
  } else if (sec == 60) {
    sec = 0;
    min++;
    get("min").innerHTML = min;
  } else if (min == 60) {
    min = 0;
  }
};
get("startButton").onclick = () => {
  if (!flag) {
    intervalHandler = setInterval(count, 10);
    flag = true;
    let green = document.getElementById("container-stopwatch");
    green.style.backgroundColor = "rgba(39, 211, 76)";
  }
};
get("stopButton").onclick = () => {
  clearInterval(intervalHandler);

  flag = false;
  let red = document.getElementById("container-stopwatch");
  red.style.backgroundColor = "rgba(231, 40, 40)";
};
get("resetButton").onclick = () => {
  get("min").textContent = "00";
  get("sec").textContent = "00";
  get("milisec").textContent = "00";
  min = 0;
  sec = 0;
  milisec = 0;
  // flag = false;
  let silver = document.getElementById("container-stopwatch");
  silver.style.backgroundColor = "rgba(88, 88, 88, 0.5)";
};

/*
Реалізуйте програму перевірки телефону
* Використовуючи JS Створіть поле для введення телефону та кнопку збереження
* Користувач повинен ввести номер телефону у форматі 000-000-00-00
* Після того як користувач натискає кнопку зберегти перевірте правильність введення номера, 
якщо номер правильний зробіть зелене тло і використовуючи document.location переведіть користувача 
на сторінку https://risovach.ru/upload/2013/03/mem/toni-stark_13447470_big_.jpeg якщо буде помилка, 
відобразіть її в діві до input.
*/

let input = document.createElement("input");
input.placeholder = "000-000-00-00";
input.classList.add("phone");
document.body.append(input);
let save = document.createElement("button");
save.classList.add("save");
document.body.append(save);
save.textContent = "SEND";
let forms = /\d{3}-\d{3}-\d{2}-\d{2}/;
save.onclick = function () {
  if (forms.test(input.value)) {
    save.style.backgroundColor = "green";
    input.style.border = "4px solid green";
    document.location =
      " https://risovach.ru/upload/2013/03/mem/toni-stark_13447470_big_.jpeg";
  } else {
    input.style.border = "5px solid red";
    save.style.backgroundColor = "red";
  }
};

/*
Слайдер
Створіть слайдер кожні 3 сек змінюватиме зображення
Зображення для відображення
https://new-science.ru/wp-content/uploads/2020/03/4848-4.jpg
https://universetoday.ru/wp-content/uploads/2018/10/Mercury.jpg
https://naukatv.ru/upload/files/shutterstock_418733752.jpg
https://cdn.iz.ru/sites/default/files/styles/900x506/public/news-2018-12/20180913_zaa_p138_057.jpg
https://nnst1.gismeteo.ru/images/2020/07/shutterstock_1450308851-640x360.jpg
*/

const images = document.querySelectorAll(".slider .slider-line .slider-image");
let sliderLine = document.querySelector(".slider-line");
let width;
function init() {
  console.log("resize");
  width = document.querySelector(".slider").offsetWidth;
  sliderLine.style.width = width * images.length + "px";
  images.forEach((item) => {
    item.style.width = width + "px";
    item.style.height = "auto";
  });
}

let i = 0;
setInterval(() => {
  if (i < 5) {
    sliderLine.style.left = `-${i * width}px`;
    i++;
    sliderLine.style.transition = "all ease-out 1s";
  } else if (i >= 5) {
    i = 0;
    sliderLine.style.transition = "none";
    sliderLine.style.left = `-${i * width}px`;
  }
}, 2000);

window.addEventListener("resize", init);
init();
