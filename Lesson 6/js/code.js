"use strict";
/* Реалізуйте клас Worker (Працівник), який матиме такі властивості: name (ім'я), surname (прізвище),
rate (ставка за день роботи), days (кількість відпрацьованих днів).
Також клас повинен мати метод getSalary(), який виводитиме зарплату працівника.
Зарплата - це добуток (множення) ставки rate на кількість відпрацьованих днів days.*/
class Worker {
  constructor(name, surname, rate, days) {
    this.name = name;
    this.surname = surname;
    this.rate = rate;
    this.days = days;
  }
  getSalary(rate, days) {
    let result = this.rate * this.days;
    return result;
  }
}
let Worker1 = new Worker("Alex", "Alekseev", 500, 25);
document.write(Worker1.name + "</br>");
document.write(Worker1.surname + "</br>");
document.write(Worker1.rate + "</br>");
document.write(Worker1.days + "</br>");
document.write(Worker1.getSalary());
document.write("</br>");
document.write("</br>");
/*
Реалізуйте клас MyString, який матиме такі методи: метод reverse(),
який параметром приймає рядок, а повертає її в перевернутому вигляді, метод ucFirst(),
який параметром приймає рядок, а повертає цей же рядок, зробивши його першу літеру великою
та метод ucWords, який приймає рядок та робить заголовною першу літеру кожного слова цього рядка.*/

class MyString {
  reverse(str) {
    let newStr = " ";
    for (let i = str.length - 1; i >= 0; i--) {
      newStr = newStr + str[i];
    }
    return newStr;
  }
  ucFirst(str) {
    let newString = str[0].toUpperCase() + str.slice(1);
    return newString;
  }
  ucWords(str) {
    let array = str.split(" ");
    let strArr = [];
    for (let i = 0; i < array.length; i++) {
      let a = array[i];
      let newStr = a[0].toUpperCase() + a.slice(1);
      strArr.push(newStr);
    }
    return strArr.join(" ");
  }
}
let str = new MyString();
document.write(str.reverse("Hello my world"));
document.write("</br>");
document.write(str.ucFirst("Hello my world"));
document.write("</br>");
document.write(str.ucWords("Hello my world"));
document.write("</br>");
document.write("</br>");
/*Створіть клас Phone, який містить змінні number, model і weight.
Створіть три екземпляри цього класу.
Виведіть на консоль значення їх змінних.
Додати в клас Phone методи: receiveCall, має один параметр - ім'я. Виводить на консоль повідомлення "Телефонує {name}". 
Метод getNumber повертає номер телефону. Викликати ці методи кожного з об'єктів.*/
class Phone {
  constructor(number, model, weight) {
    this.number = number;
    this.model = model;
    this.weight = weight;
  }
  receiveCall(name) {
    return `Телефонує ${name}`;
  }
  getNumber(number) {
    return `Номер телефону ${number}`;
  }
}
let MyTel1 = new Phone("0961111111", "X9", "200");
document.write(
  MyTel1.number + "</br>" + MyTel1.model + "</br>" + MyTel1.weight + "</br>"
);
document.write(MyTel1.receiveCall("Alex"));
document.write("</br>");
document.write(MyTel1.getNumber(MyTel1.number));
document.write("</br>");
let MyTel2 = new Phone("0632222222", "X10", "250");
document.write(
  MyTel2.number + "</br>" + MyTel2.model + "</br>" + MyTel2.weight + "</br>"
);
document.write(MyTel2.receiveCall("Oleg"));
document.write("</br>");
document.write(MyTel2.getNumber(MyTel2.number));
document.write("</br>");
let MyTel3 = new Phone("0503333333", "X12", "300");
document.write(
  MyTel3.number + "</br>" + MyTel3.model + "</br>" + MyTel3.weight + "</br>"
);
document.write(MyTel3.receiveCall("Pavel"));
document.write("</br>");
document.write(MyTel3.getNumber(MyTel3.number));
document.write("</br>");
document.write("</br>");
/*
Створити клас Car , Engine та Driver.
Клас Driver містить поля - ПІБ, стаж водіння.
Клас Engine містить поля – потужність, виробник.
Клас Car містить поля – марка автомобіля, клас автомобіля, вага, водій типу Driver, мотор типу Engine. 
Методи start(), stop(), turnRight(), turnLeft(), які виводять на друк: "Поїхали", "Зупиняємося", "Поворот праворуч" або "Поворот ліворуч". 
А також метод toString(), який виводить повну інформацію про автомобіль, її водія і двигуна. */
class Car {
  constructor(carBrand, carClass, weight, driver, engine) {
    this.carClass = carClass;
    this.carBrand = carBrand;
    this.weight = weight;
    this.driver = driver;
    this.engine = engine;
  }

  start() {
    return "Поїхали";
  }

  stop() {
    return "Зупиняємось";
  }
  turnRight() {
    return "Поворот праворуч";
  }
  turnLeft() {
    return "Поворот ліворуч";
  }
  toString() {
    return `Класс: ${this.carClass}, Бренд: ${this.carBrand},Вага: ${this.weight}, Водій: ${this.driver.fullname}, Стаж Водія: ${this.driver.drivingExperience}, Потужність Двигуна: ${this.engine.power}, Виробник Двигуна: ${this.engine.producer}`;
  }
}
class Engine {
  constructor(power, producer) {
    this.power = power;
    this.producer = producer;
  }
}
class Driver {
  constructor(drivingExperience, fullname) {
    this.drivingExperience = drivingExperience;
    this.fullname = fullname;
  }
}
/*Створити похідний від Car клас - Lorry (вантажівка), що характеризується також вантажопідйомністю кузова.
Створити похідний від Car клас - SportCar, який також характеризується граничною швидкістю. */
class Lorry extends Car {
  constructor(carry) {
    super();
    this.carry = carry;
  }
}
class SportCar extends Car {
  constructor(speed) {
    super();
    this.speed = speed;
  }
}
let mashine = new Car();
document.write(mashine.start());
document.write("</br>");
let driver = new Driver("10", "Vasia Pupkin");
let engine = new Engine("200", "BMW");
let auto = new Car("BMW", "light", "1500", driver, engine);
document.write(auto.toString());
document.write("</br>");
let truck = new Lorry("3000 тон");
document.write(truck.carry);
document.write("</br>");
let fast = new SportCar("160 км/ч");
document.write(fast.speed);
document.write("</br>");
document.write("</br>");
/*
Створити клас Animal та розширюючі його класи Dog, Cat, Horse.
Клас Animal містить змінні food, location і методи makeNoise, eat, sleep. Метод makeNoise, наприклад, може виводити на консоль "Така тварина спить".
Dog, Cat, Horse перевизначають методи makeNoise, eat.
Додайте змінні до класів Dog, Cat, Horse, що характеризують лише цих тварин.*/
class Animal {
  constructor(food, location) {
    this.food = food;
    this.location = location;
  }
  makeNoise() {
    return "Тварина шумить";
  }
  eat() {
    return "Тварина їсть";
  }
  sleep() {
    return "Тварина спить";
  }
}

class Dog extends Animal {
  constructor(food, location) {
    super(food, location);
  }
  makeNoise() {
    return "gaf-gaf";
  }
  eat() {
    return "їсть м'ясо";
  }
}
class Cat extends Animal {
  constructor(food, location) {
    super(food, location);
  }
  makeNoise() {
    return "may-may";
  }
  eat() {
    return "П'є молоко";
  }
}
class Horse extends Animal {
  constructor(food, location) {
    super(food, location);
  }
  makeNoise() {
    return "igo-go";
  }
  eat() {
    return "Їсть сіно";
  }
}
/*Створіть клас Ветеринар, у якому визначте метод void treatAnimal(Animal animal). Нехай цей метод роздруковує food і location тварини, що прийшла на прийом.
У методі main створіть масив типу Animal, в який запишіть тварин всіх типів, що є у вас. У циклі надсилайте їх на прийом до ветеринара.*/
class Vet {
  constructor(food, location) {
    this.food = food;
    this.location = location;
  }
  treatAnimal(animal) {
    return animal.food, animal.location;
  }
  main() {
    let allanimals = new Animal();
    allanimals = [Dog, Cat, Horse];
    for (let i = 0; i < allanimals.length; i++) {
      console.log(allanimals[i]);
    }
  }
}

const Vet2 = new Vet();
Vet2.main();
let Dog2 = new Dog("meat", "Rivne");
document.write(Dog2.food + "</br>" + Dog2.location);
document.write("</br>");
document.write(Dog2.makeNoise());
document.write("</br>");
