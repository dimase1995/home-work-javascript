/* Создать объект "Документ", в котором определить свойства "Заголовок, тело, футер, дата". Создать в объекте вложенный объект - "Приложение". 
Создать в объекте "Приложение", вложенные объекты, "Заголовок, тело, футер, дата". Создать методы для заполнения и отображения документа.*/
var doc = {
  header: "Написати заголовок!",
  body: "Написати контент!",
  footer: "Створити футер!",
  date: "Дата написання!",
  show: function () {
    alert(
      "Створіть документ: " +
        "\rheader: " +
        this.header +
        "\rbody: " +
        this.body +
        "\rfooter: " +
        this.footer +
        "\rdate: " +
        this.date
    );
  },
  application: {
    title: {
      heading: function (text1) {
        document.getElementById("header").innerHTML = text1;
      },
    },
    content: {
      page: function (text2) {
        document.getElementById("content").innerHTML = text2;
      },
    },
    footer: {
      botoom: function (text3) {
        document.getElementById("footer").innerHTML = text3;
      },
    },
    date: {
      data: function (text4) {
        document.getElementById("date").innerHTML = text4;
      },
    },
  },
};
doc.show();
doc.application.title.heading(
  prompt("Напишіть заголовок документа!", "header")
);
doc.application.content.page(prompt("Напишіть контент документа!", "body"));
doc.application.footer.botoom(prompt("Напишіть футер документа!", "footer"));
doc.application.date.data(prompt("Дата написання!", "Січень 2023"));
/*
var document = {
  header: "Title",
  body: "Тіло",
  footer: "Футер",
  date: "Дата",
  show: function (text) {
    document.getElementById(
      "header"
    ).innerHTML = `<div>Заголовок: ${document.header} </div>`;
  },
  
  application: {
    header: {};
    body: {};
    footer: {};
    date: {};
  }
  
};

document.show(prompt("Напищіть заголовок документа, header"));
*/
