/*
Зробити програму з навігаційним меню яке буде показувати один з варіантів. 

Курс валют НБУ з датою на який день, 

героїв зоряних війн, 

список справ з https://jsonplaceholder.typicode.com/ 

*/
let result = [];
let urls = [
  "https://swapi.dev/api/planets/?format=json",
  "https://swapi.dev/api/planets/?page=2&format=json",
  "https://swapi.dev/api/planets/?page=3&format=json",
  "https://swapi.dev/api/planets/?page=4&format=json",
  "https://swapi.dev/api/planets/?page=5&format=json",
  "https://swapi.dev/api/planets/?page=6&format=json",
];
const req = async (url) => {
  document.querySelector(".box_loader").classList.add("show");
  const data = await fetch(url);
  return await data.json();
};

const nav = document.querySelector(".nav").addEventListener("click", (e) => {
  if (e.target.dataset.link === "nbu") {
    req(
      "https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json"
    ).then((info) => {
      show(info);
      console.log(info);
    });
  } else if (e.target.dataset.link === "star") {
    urls.forEach((el) => {
      req(el).then((info) => {
        result.push(...info.results);
        show(result);
        //
        // console.log(info.results);
        // console.log(result);
      });
    });

    // вивести всі 60 планет з https://swapi.dev/api/planets/
  } else if (e.target.dataset.link === "todo") {
    req("https://jsonplaceholder.typicode.com/todos").then((info) => {
      show(info);
      console.log(info);
    });
  } else {
  }
});

function show(data = []) {
  if (!Array.isArray(data)) return;

  const tbody = document.querySelector("tbody");
  tbody.innerHTML = "";
  const newArr = data.map(
    (
      { txt, rate, exchangedate, title, completed, name, diameter, climate },
      i
    ) => {
      return {
        id: i + 1,
        name: txt || title || name,
        info1: rate || completed || diameter,
        info2: exchangedate || climate || "тут пусто",
      };
    }
  );

  newArr.forEach(({ name, id, info1, info2 }) => {
    tbody.insertAdjacentHTML(
      "beforeend",
      `
        <tr> 
        <td>${id}</td>
        <td>${name}</td>
        <td>${info1}</td>
        <td>${info2}</td>
        </tr>
        `
    );
  });

  document.querySelector(".box_loader").classList.remove("show");
}
