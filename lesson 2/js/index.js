/*
Використовуючи циклічні конструкції, пробіли (&nbsp;) та зірки (*) намалюйте:
Порожній 
- прямокутник 
Заповнений 
- Рівнобедрений трикутник 
- Трикутник прямокутний
- Ромб
    */

// Трикутник прямокутний
for (let i = 0; i < 10; i++) {
  for (let k = i; k < 10; k++) {
    document.write(" ");
  }
  for (let j = 1 + i; j > 0; j--) {
    document.write("⭐");
  }
  document.write("<br/>");
}
document.write("<br/>");
document.write("<br/>");
// Рівнобедрений трикутник
for (let i = 0; i < 10; i++) {
  for (let k = i; k < 10; k++) {
    document.write("&nbsp&nbsp");
  }
  for (let j = 1 + i; j > 0; j--) {
    document.write("⭐");
  }
  document.write("<br/>");
}
document.write("<br/>");
document.write("<br/>");
// Ромб
for (let i = 0; i < 8; i++) {
  for (let k = i; k < 8; k++) {
    document.write(" &nbsp&nbsp ");
  }
  for (let j = 1 + i; j > 0; j--) {
    document.write("⭐");
  }
  document.write("<br/>");
}
for (let i = 0; i < 8; i++) {
  for (let k = 1 + i; k > 0; k--) {
    document.write(" &nbsp&nbsp ");
  }
  for (let j = i; j < 8; j++) {
    document.write("⭐");
  }

  document.write("<br/>");
}
document.write("<br/>");
document.write("<br/>");
// прямокутник
for (let l = 0; l < 16; l++) {
  document.write("⭐");
}
document.write("<br/>");
for (let i = 0; i < 8; i++) {
  for (let k = 0; k < 15; k++) {
    if (k === 0 || k === 14) {
      document.write("⭐");
    } else {
      document.write("&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp");
    }
  }
  document.write("<br/>");
}
for (let l = 0; l < 16; l++) {
  document.write("⭐");
}
